<?php

use Respect\Validation\Validator as v;

session_start();

require_once __DIR__ . '/../vendor/autoload.php';

$config = require_once (__DIR__ . '/../config.php');

$app = new Slim\App($config);

$container = $app->getContainer();

$capsule = new Illuminate\Database\Capsule\Manager;
$capsule->addConnection($container['settings']['db']);
$capsule->setAsGlobal();
$capsule->bootEloquent();

$container['db'] = function () use ($capsule){
    return $capsule;
};

$container['view'] = function ($container) {

    $view = new \Slim\Views\Twig('../app/resources/views', [
        'cache' => false
    ]);

    // Instantiate and add Slim specific extension
    $basePath = rtrim(str_ireplace('index.php', '', $container['request']->getUri()->getBasePath()), '/');
    $view->addExtension(new Slim\Views\TwigExtension($container['router'], $basePath));

    return $view;
};

$container['csrf'] = function (){
    return new Slim\Csrf\Guard;
};

$container['mailer'] = function (){
    return new PHPMailer;
};

$container['mail_util'] = function ($container){
    return new App\Utils\Mail($container);
};

$container['auth'] = function (){
    return new App\Auth\Auth;
};
$container['validator'] = function (){
    return new App\Validation\Validator;
};

$container['UserController'] = function($container) {
    return new App\Controllers\UserController($container);
};

$container['HomeController'] = function($container) {
    return new App\Controllers\HomeController($container);
};

$container['LoginController'] = function($container) {
    return new App\Controllers\LoginController($container);
};

$container['IntegrationController'] = function($container) {
    return new App\Controllers\IntegrationController($container);
};

$container['GridController'] = function($container) {
    return new App\Controllers\GridController($container);
};
$container['ItemController'] = function($container) {
    return new App\Controllers\ItemController($container);
};

$container['PasswordController'] = function($container) {
    return new App\Controllers\PasswordController($container);
};

$container['Clipboard'] = function($container) {
    return new App\Clipboard\Clipboard($container);
};

$app->add(new App\Middleware\ValidationErrorsMiddleware($container));
$app->add(new App\Middleware\OldInputMiddleware($container));
$app->add(new App\Middleware\CsrfViewMiddleware($container));
$app->add(new App\Middleware\AuthViewMiddleware($container));
$app->add($container->csrf);

v::with('\\App\\Validation\Rules\\');

require_once __DIR__ . '/../app/routes.php';