<?php
return [
    'settings' => [
        'displayErrorDetails' => false,
        'base_url' => 'http://dumb-specwriter/',
        'db' => [
            'driver' => 'mysql',
            'database' => 'dumb_specwriter',
            'host' => 'localhost',
            'username' => 'root',
            'password' => 'root',
            'charset' => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix' => ''
        ],
    ],

];
?>