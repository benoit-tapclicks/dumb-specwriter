<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    protected $primaryKey = 'user_id';

    protected $guarded = [];

    public function integration()
    {
        return $this->hasOne('App\Models\Integration', 'user_id');
    }
}