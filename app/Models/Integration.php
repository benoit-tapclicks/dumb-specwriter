<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Integration extends Model
{
    protected $primaryKey = 'integration_id';

    protected $guarded = [];

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }

    public function grids()
    {
        return $this->hasMany('App\Models\Grid', 'integration_id');
    }
}