<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Grid extends Model
{
    protected $primaryKey = 'grid_id';

    protected $guarded = [];

    public function integration()
    {
        return $this->belongsTo('App\Models\Integration', 'integration_id');
    }

    public function items()
    {
        return $this->hasMany('App\Models\Item', 'grid_id');
    }
}