<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    protected $primaryKey = 'item_id';

    protected $guarded = [];

    public function grids()
    {
        return $this->belongsTo('App\Models\Grid', 'grid_id');
    }
}