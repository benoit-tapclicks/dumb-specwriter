<?php

namespace App\Clipboard;
use App\Models\Grid;
use App\Models\Item;
use App\Models\User;

class Clipboard
{
    public function __construct($container)
    {
        $this->auth = $container->get('auth');
    }

    public function copy($request, $response)
    {
        unset($_SESSION['clipboard']['items']);

        foreach ($request->getParam('ids') as $item_id)
        {
            $_SESSION['clipboard']['items'][] = $item_id;
        }

        return $response->withJSON(json_encode($_SESSION['clipboard']['items']));
    }
    public function paste($request, $response)
    {
        $grid_id = $request->getParam('grid_id');

        $grids = User::findOrFail($this->auth->getUserId())
            ->integration()->firstOrFail()
            ->grids ;

        foreach ($grids as $grid)
        {
            $grid_ids[] = $grid->grid_id;
        }

        if(!in_array($grid_id, $grid_ids))
        {
            return $response->withJSON(json_encode(['error' => true, 'message' => 'Invalid request']));
        }

        $items = Item::whereIn('grid_id', $grid_ids)->get();

        foreach ($items as $item)
        {
            if(in_array($item->item_id, $_SESSION['clipboard']['items']))
            {
                Item::create([
                    'grid_id' => $grid_id,
                    'label' => $item->label,
                    'active' => $item->active,
                    'api_index' => $item->api_index,
                    'format' => $item->format,
                    'formula' => $item->formula,
                    'is_dashboard_metric' => $item->is_dashboard_metric,
                    'is_dimension' => $item->is_dimension,
                    'is_metric' => $item->is_metric,
                    'is_top_metric' => $item->is_top_metric,
                    'max_precision' => $item->max_precision,
                    'note' => $item->note,
                    'parent_level' => $item->parent_level,
                ]);
            }
        }

        return $response->withJson(json_encode($items));
    }
}