<?php

namespace App\Utils;

class StringUtil
{
    public static function fieldToError($string)
    {
         return ucfirst(str_replace('_', ' ', $string));
    }
}