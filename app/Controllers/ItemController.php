<?php
namespace App\Controllers;

use App\Models\Grid;
use App\Models\Item;
use App\Models\User;
use Respect\Validation\Validator as v;

class ItemController extends BaseController
{
    public function index($request, $response, $args)
    {
        $grid_id = $args['grid_id'];
        $integration = User::findOrFail($this->auth->getUserId())->integration()->first();

        if(is_null($integration))
        {
            $_SESSION['alerts'][] = [
                "message" => "You need to create an integration before to add new data levels.",
                "type" => "danger"
            ];

            return $response->withRedirect('/integration');
        }

        $integration->grids()->findOrFail($grid_id);
        $grids = $integration->grids()->where('integration_id', $integration->integration_id)->get();
        $items = Item::where('grid_id', $grid_id)->get()->sortBy('sort_order');
        $data['items'] = $items;
        $data['grids'] = $grids;
        $data['current_grid_id'] = $grid_id;

        return $this->view->render($response, 'item/index.twig', $data);
    }

    public function insert($request, $response, $args)
    {
        $validation = $this->validator->validate($request, [
            'grid_id' => v::hasAccessGrid(),
        ]);

        if($validation->failed())
        {
            return $response->withJSON(json_encode(['error' => true, 'message' => array_values($_SESSION['errors'])]));
        }

        $item = Item::create([
            'grid_id' => $request->getParam('grid_id'),
            'is_metric' => 1
        ]);

        ob_start();
        require ROOT . 'app/resources/views/item/row.php';
        $row = ob_get_contents();
        ob_end_clean();

        return $response->withJSON(json_encode($row));
    }
    public function update($request, $response, $args)
    {

    }


    public function delete($request, $response)
    {
        $validation = $this->validator->validate($request, [
            'grid_id' => v::hasAccessGrid(),
        ]);

        if($validation->failed())
        {
            return $response->withJSON(json_encode(['error' => true, 'message' => array_values($_SESSION['errors'])]));
        }

        $items = $request->getParam('items');
        $grid_id = $request->getParam('grid_id');
        foreach ($items as $item_id)
        {
            $item = Item::where(['item_id' => $item_id, 'grid_id' => $grid_id]);
            $item->delete();
        }

        return $response->withJSON(json_encode(['error' => false, 'items' => $items]));
    }

    public function sort($request, $response, $args)
    {
        $validation = $this->validator->validate($request, [
            'grid_id' => v::hasAccessGrid(),
        ]);

        if($validation->failed())
        {
            return $response->withJSON(json_encode(['error' => true, 'message' => array_values($_SESSION['errors'])]));
        }


        $params = $request->getParams();
        foreach ($params['order'] as $index => $item_id)
        {
            Item::where(['item_id' => $item_id, 'grid_id' => $args['grid_id']])->update(['sort_order' => $index]);
        }
    }

    public function yn($request, $response, $args)
    {
        $validation = $this->validator->validate($request, [
            'grid_id' => v::hasAccessGrid()
        ]);

        if($validation->failed())
        {
            return $response->withJSON(json_encode(['error' => true, 'message' => array_values($_SESSION['errors'])]));
        }

        $valid_fields = [
            'is_dimension',
            'is_metric',
            'is_dashboard_metric',
            'is_top_metric',
            'active'
        ];

        $radio = [
            'is_metric' => 'is_dimension',
            'is_dimension' => 'is_metric'
        ];
        $radio_value = [1,0];

        $field = $request->getParam('field');
        $yn = $request->getParam('yn');
        $item_id = $request->getParam('item_id');

        if(in_array($field, $valid_fields))
        {

            if(isset($radio[$field]))
            {
                $update = [
                    $field => $yn,
                    $radio[$field] => $radio_value[$yn]
                ];
            }
            else
            {
                $update = [$field => $yn];
            }

            Item::where(['item_id' => $item_id, 'grid_id' => $args['grid_id']])->update($update);

            return $response->withJSON(json_encode(['error' => false]));
        }
    }

    public function edit($request, $response, $args)
    {
        $validation = $this->validator->validate($request, [
            'grid_id' => v::hasAccessGrid()
        ]);

        if($validation->failed())
        {
            return $response->withJSON(json_encode(['error' => true, 'message' => array_values($_SESSION['errors'])]));
        }

        $valid_fields = [
            'label',
            'api_index',
            'formula',
            'format',
            'max_precision',
            'parent_level',
            'note'
        ];

        $field = $request->getParam('field');
        $grid_id = $request->getParam('grid_id');
        $item_id = $request->getParam('item_id');
        $value = $request->getParam('value');

        if(!in_array($field, $valid_fields))
        {
            return $response->withJSON(json_encode(['error' => true, 'message' => array_values($_SESSION['errors'])]));
        }

        Item::where(['grid_id' => $grid_id, 'item_id' => $item_id])->update([$field => $value]);

        return $response->withJSON(json_encode(['error' => false]));
    }
}
?>