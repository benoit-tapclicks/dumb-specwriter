<?php

namespace App\Controllers;

use App\Models\Integration;
use App\Models\User;
use App\Models\Grid;

class GridController extends BaseController
{
    public function index($request, $response)
    {
        $integration = User::findOrFail(['user_id' => $this->auth->getUserId()])->first()
            ->integration()->first();

        if(is_null($integration))
        {
            $_SESSION['alerts'][] = [
                "message" => "You need to create an integration before to add new data levels.",
                "type" => "danger"
            ];

            return $response->withRedirect('/integration');
        }

        $data['grids'] = $integration->grids;

        foreach ($data['grids'] as $grid)
        {
            $data['parents'][$grid->grid_id] = $grid->grid_name;
        }

        return $this->view->render($response, 'grid/index.twig', $data);
    }

    public function create($request, $response, $args)
    {
        $grid_id = $args['grid_id'] ?? null;
        $integration = User::findOrFail(['user_id' => $this->auth->getUserId()])->first()->integration()->first();
        $grids = $integration->grids;

        $data['grids'] = $grids;
        $data['grid'] = $grids->find($grid_id);

        return $this->view->render($response, 'grid/form.twig', $data);
    }

    public function save($request, $response, $args)
    {
        $integration = User::findOrFail($this->auth->getUserId())->integration()->first();

        if(is_null($integration))
        {
            $_SESSION['alerts'][] = [
                "message" => "You need to create an integration before to add new data levels.",
                "type" => "danger"
            ];

            return $response->withRedirect('/integration');
        }

        $this->db->getConnection()->transaction(function() use ($integration, $request, $args){

            $grid_id = $args['grid_id'] ?? null;
            $grid = $integration->grids()->findOrNew($grid_id);
            $update_parent_id = ($grid->is_cgn && $grid_id && $request->getParam('is_cgn') != '1');
            $grid->integration_id = $integration->integration_id;
            $grid->grid_name = $request->getParam('grid_name');
            $grid->is_cgn = ($request->getParam('is_cgn') == '1');

            if($request->getParam('is_cgn') != '1')
            {
                $grid->parent_grid_id = $request->getParam('parent_grid_id');
            }

            $grid->save();

            if($update_parent_id)
            {
                Grid::where('parent_grid_id', $grid_id)->update(['parent_grid_id' => null]);
            }
        });


        return $response->withRedirect('/grid');
    }

    public function delete($request, $response, $args)
    {
        $this->db->getConnection()->transaction(function() use ($args){

            $integrations = Integration::where(['user_id' => $this->auth->getUserId()])->firstOrFail();
            $grid = Grid::where([
                ['integration_id', $integrations->integration_id],
                ['grid_id', $args['grid_id']]
            ])->firstOrFail();

            Grid::where('parent_grid_id', $grid->grid_id)->update(['parent_grid_id' => null]);
            Grid::destroy($grid->grid_id);

        });

        return $response->withRedirect('/grid');
    }

}