<?php

namespace App\Controllers;

use App\Models\User;
use Respect\Validation\Validator as v;

class UserController extends BaseController
{
    public function index($request, $response)
    {
        $data['users'] = User::where('role', '!=', 'admin')->get();
        $this->view->render($response, 'users/index.twig', $data);
    }

    public function getInvite($request, $response)
    {
        $this->view->render($response, 'users/invite.twig');
    }
    public function postInvite($request, $response)
    {
        $validation = $this->validator->validate($request, [
            'name' => v::notEmpty(),
            'company_name' => v::notEmpty(),
            'email' => v::email()->not(v::EmailExists()),
        ]);

        if($validation->failed())
        {
            return $response->withRedirect('/admin/users/invite');
        }

        $user = User::create([
            'name' => $request->getParam('name'),
            'company_name' => $request->getParam('company_name'),
            'email' => $request->getParam('email'),
            'role' => 'client',
            'status' => 'invited',
            'token' => bin2hex(openssl_random_pseudo_bytes(16)),
            'token_expiration' => time() + 60 * 60 * 24 * 7
        ]);

        $object = 'Integration Application';
        $body = $this->settings->get('base_url') . "password/$user->token/$user->email";

        if($this->mail_util->send($object, $body, $user->email))
        {
            $_SESSION['alerts'][] = [
                "message" => "An invitation email has been sent to $user->email",
                "type" => "success"
            ];
            return $response->withRedirect('/admin/users');
        }
        else
        {
            $_SESSION['errors']['email_sent'] = "Email to $user->email could not be sent.";
            $user->delete();
            return $response->withRedirect('/admin/users/invite');
        }

    }

    public function delete($request, $response)
    {
        //@todo remove foreign key on integration to prevent client lost his work if admin remove user.
        User::find($request->getParam('user_id'))->delete();

        $data = [
            'status' => 'success',
            'user_id' => $request->getParam('user_id')
        ];
        return $response->withJSON(json_encode($data));
    }
}