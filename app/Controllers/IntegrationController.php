<?php

namespace App\Controllers;

use App\Models\User;

class IntegrationController extends BaseController
{
    public function index($request, $response)
    {
        $integration = User::findOrFail($this->auth->getUserId())
            ->integration()
            ->first();
        $data['integration'] = [
            'name' => $integration->name,
            'feed' => $integration->feed,
            'chart_type' => $integration->chart_type,
        ];

        return $this->view->render($response, 'integration/index.twig', $data);
    }

    public function create($request, $response)
    {
        $integration = User::findOrFail($this->auth->getUserId())
            ->integration()
            ->first();
        $data = [
            'name' => $integration->name,
            'feed' => $integration->feed,
            'chart_type' => $integration->chart_type
        ];

        return $this->view->render($response, 'integration/form.twig', $data);
    }

    public function save($request, $response)
    {
        $integration = User::findOrFail($this->auth->getUserId())
            ->integration()
            ->firstOrNew(['user_id' => $this->auth->getUserId()]);
        $integration->name = $request->getParam('name');
        $integration->feed = $request->getParam('feed');
        $integration->chart_type = $request->getParam('chart_type');
        $integration->save();

        return $response->withRedirect('/integration');
    }


}