<?php

namespace App\Controllers;

class LoginController extends BaseController
{
    public function index($request, $response)
    {
        return $this->view->render($response, 'login/index.twig');
    }

    public function login($request, $response)
    {
        if($this->auth->attempt($request->getParam('email'), $request->getParam('pass')))
        {
            $redirect = ($_SESSION['user']['role'] === 'admin') ? '/admin/users' : '/';
            return $response->withRedirect($redirect);
        }

        $_SESSION['errors']['login'][] = 'Can\'t connect with these credentials';

        return $response->withRedirect('/login');
    }

    public function logout($request, $response)
    {
        $this->auth->logout();

        return $response->withRedirect('/');
    }
}