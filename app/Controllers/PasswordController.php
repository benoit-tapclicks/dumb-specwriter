<?php

namespace App\Controllers;

use App\Models\User;

class PasswordController extends BaseController
{
    public function index($request, $response, $args)
    {
        $user = User::where([
            'email' => $args['email'],
            'token' => $args['token'],
        ])->first();

        if(!$user || empty($args['email']) || empty($args['token']))
        {
            $notFoundHandler = $this->container->get('notFoundHandler');
            return $notFoundHandler($request, $response);
        }

        $data['token_expired'] = ($user->token_expiration < time()) ? 1 : 0;
        $data['token'] = $args['token'];
        $data['email'] = $args['email'];

        return $this->view->render($response,'password/index.twig',$data);
    }

    public function update($request, $response)
    {
        $token = $request->getParam('token');
        $email = $request->getParam('email');
        $pass_1 = $request->getParam('pass_1');
        $pass_2 = $request->getParam('pass_2');

        $user = User::where([
            'email' => $email,
            'token' => $token,
        ])->first();

        if(!$user || empty($email) || empty($token))
        {
            $_SESSION['errors'][] = "This link is invalid";
        }

        if($user->token_expiration < time())
        {
            $_SESSION['errors'][] = "This link has expired";
        }

        if(strlen($pass_1) < 7)
        {
            $_SESSION['errors'][] = "Password must be at least 7 characters long";
        }

        if($pass_1 != $pass_2)
        {
            $_SESSION['errors'][] = "Passwords don't match";
        }

        if($_SESSION['errors'])
        {
            return $response->withRedirect("/password/$token/$email");
        }

        $user->pass = hash('sha256', $pass_1);
        $user->status = 'active';
        $user->token = '';
        $user->token = '';
        $user->token_expiration = 0;
        $user->save();

        if($this->auth->attempt($email, $pass_1))
        {
            $_SESSION['alerts'][] = [
                "message" => "You are now logged in as $email",
                "type" => "success"
            ];

            return $response->withRedirect('/integration');
        }
        else
        {
            $_SESSION['errors']['login'][] = 'Please login with your email address and password.';
            return $response->withRedirect('/login');
        }
    }
}