<?php

namespace App\Controllers;

abstract class BaseController
{
    public function __construct($container)
    {
        $this->view = $container->get('view');
        $this->auth = $container->get('auth');
        $this->db = $container->get('db');
        $this->router = $container->get('router');
        $this->validator = $container->get('validator');
        $this->mail_util = $container->get('mail_util');
        $this->settings = $container->get('settings');
        $this->container = $container;
    }
}
