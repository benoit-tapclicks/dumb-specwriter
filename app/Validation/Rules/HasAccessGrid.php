<?php

namespace App\Validation\Rules;

use Respect\Validation\Rules\AbstractRule;

use App\Auth\Auth;
use App\Models\User;

class HasAccessGrid extends AbstractRule
{
    public function validate($grid_id)
    {
        $user_id =  (new Auth())->getUserId();
        $grid = User::find($user_id)
            ->integration()->where('user_id', $user_id)->first()
            ->grids()->find($grid_id);

        return !is_null($grid);
    }
}