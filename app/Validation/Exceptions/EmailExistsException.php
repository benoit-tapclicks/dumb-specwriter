<?php

namespace App\Validation\Exceptions;

use Respect\Validation\Exceptions\ValidationException;

class EmailExistsException extends ValidationException
{
    public static $defaultTemplates = [
        self::MODE_DEFAULT => [
            self::STANDARD => 'Email doesn\'t exists',
        ],
        self::MODE_NEGATIVE => [
            self::STANDARD => 'Email is already taken',
        ]
    ];
}