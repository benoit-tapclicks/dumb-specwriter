<?php $output  = '<tr data-item_id="' . $item->item_id . '">
            <td class="table-cb"><input class="cb" type="checkbox"></td>
            <td class="editable" data-field="label"></td>
            <td class="editable" data-field="api_index"></td>
            <td class="text-center yn" data-field="active"></td>
            <td class="text-center yn" data-field="is_dimension"></td>
            <td class="text-center yn" data-field="is_metric"><span class="text-success glyphicon glyphicon-ok"></span></td>
            <td class="text-center yn" data-field="is_dashboard_metric"></td>
            <td class="text-center yn" data-field="is_top_metric"></td>
            <td class="editable" data-field="formula"></td>
            <td class="editable" data-field="format"></td>
            <td class="editable" data-field="max_precision"></td>
            <td class="editable" data-field="parent_level"></td>
            <td class="editable" data-field="note"></td>
            <td class="item-action">
              <ul class="list-inline pull-right" style="margin-bottom: 0px;">
                <li>
                  <a title="New Item" data-toggle="tooltip" data-action="insert" class="btn btn-success btn-sm new">
                    <span class="glyphicon  glyphicon-plus"></span>
                  </a>
                </li>                
              </ul>
            </td>
          </tr>';

echo $output;