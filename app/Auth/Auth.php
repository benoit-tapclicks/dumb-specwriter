<?php

namespace App\Auth;

use App\Models\User;

class Auth
{
    public function getUserId()
    {
        return isset($_SESSION['user']['user_id']) ? $_SESSION['user']['user_id'] : null;
    }

    public function isAdmin()
    {
        return (!empty($_SESSION['user']['role']) && $_SESSION['user']['role'] === 'admin');
    }

    public function isLoggedIn()
    {
        return (!empty($_SESSION['login']) && $_SESSION['login'] === true);
    }

    public function logout()
    {
        unset($_SESSION['login']);
        unset($_SESSION['user']);
        session_destroy();
    }

    public function attempt($email, $pass)
    {
        $user = User::where([
            ['email', '=', $email],
            ['pass', '=', hash('sha256', $pass)],
        ])->first();

        //@todo Create validator for empty.
        if(!$user || empty($pass) || empty($email))
        {
            return false;
        }

        $_SESSION['login'] = true;
        $_SESSION['user'] = [
            'user_id' => $user->user_id,
            'name' => $user->name,
            'role' => $user->role,
            'status' => $user->status,
        ];

        return true;
    }
}