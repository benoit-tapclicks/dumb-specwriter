<?php

$app->get('/', 'HomeController:index')->setName('home');
$app->get('/login', 'LoginController:index')->setName('login');
$app->get('/password/{token}/{email}', 'PasswordController:index')->setName('password');
$app->post('/password/post', 'PasswordController:update')->setName('password.post');
$app->get('/logout', 'LoginController:logout')->setName('logout');

$app->group('', function(){
    //Integrations
    $this->get('/integration', 'IntegrationController:index')->setName('integration');
    $this->get('/integration/create', 'IntegrationController:create')->setName('integration.create');
    $this->post('/integration/save', 'IntegrationController:save')->setName('integration.save');
    $this->post('/items/clipboard/copy', 'Clipboard:copy')->setName('items.copy');
    $this->post('/items/clipboard/paste', 'Clipboard:paste')->setName('items.paste');

    $this->group('/grid', function(){
        //Grids
        $this->get('', 'GridController:index')->setName('grid');
        $this->get('/create[/{grid_id}]', 'GridController:create')->setName('grid.create');
        $this->post('/save/[{grid_id:[0-9]+}]', 'GridController:save')->setName('grid.save');
        $this->get('/delete/{grid_id:[0-9]+}', 'GridController:delete')->setName('grid.delete');
        //Items
        $this->get('/{grid_id:[0-9]+}/items', 'ItemController:index')->setName('items');
        $this->post('/{grid_id:[0-9]+}/item/insert', 'ItemController:insert')->setName('item.insert');
        $this->post('/{grid_id:}/item/update/{item_id:[0-9]+}', 'ItemController:save')->setName('item.update');
        $this->post('/{grid_id:[0-9]+}/items/delete', 'ItemController:delete')->setName('item.delete');
        $this->post('/{grid_id:[0-9]+}/items/sort', 'ItemController:sort')->setName('items.sort');
        $this->post('/{grid_id:[0-9]+}/item/yn', 'ItemController:yn')->setName('items.yn');
        $this->post('/{grid_id:[0-9]+}/item/edit', 'ItemController:edit')->setName('items.edit');
    });

})->add(new App\Middleware\AuthMiddleware($container));

$app->post('/login/post', 'LoginController:login')->setName('login.post');


$app->group('/admin', function(){
    $this->get('/users', 'UserController:index')->setName('users');
    $this->get('/users/invite', 'UserController:getInvite')->setName('invite');
    $this->post('/users/invite/post', 'UserController:postInvite')->setName('invite.post');
    $this->post('/users/delete', 'UserController:delete')->setName('user.delete');
})->add(new App\Middleware\AuthAdminMiddleware($container));
