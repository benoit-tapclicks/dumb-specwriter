<?php
namespace App\Middleware;

class Middleware
{
    public function __construct($container)
    {
        $this->container = $container;
        $this->view = $container->get('view');
        $this->csrf = $container->get('csrf');
        $this->auth = $container->get('auth');
        $this->not_found_handler = $container->get('notFoundHandler');
    }
}