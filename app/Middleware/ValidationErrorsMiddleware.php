<?php
namespace App\Middleware;

class ValidationErrorsMiddleware extends Middleware
{
    public function __invoke($request, $response, $next)
    {
        $errors = $_SESSION['errors'] ?? [];
        $alerts = $_SESSION['alerts'] ?? [];
        $this->view->getEnvironment()->addGlobal('errors', $errors);
        $this->view->getEnvironment()->addGlobal('alerts', $alerts);
        unset($_SESSION['errors']);
        unset($_SESSION['alerts']);
        $response = $next($request, $response);
        return $response;
    }
}