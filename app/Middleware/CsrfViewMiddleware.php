<?php
 namespace App\Middleware;

 class CsrfViewMiddleware extends Middleware
 {
     public function __invoke($request, $response, $next)
     {
         $this->view->getEnvironment()->addGlobal('csrf', [
            'field' => '
                <input id="token_name" type="hidden" name="'. $this->csrf->getTokenNameKey() .'" value="'. $this->csrf->getTokenName() .'">
                <input id="token_value" type="hidden" name="'. $this->csrf->getTokenValueKey() .'" value="'. $this->csrf->getTokenValue() .'">
            '
         ]);

         $response = $next($request, $response);
         return $response;
     }
 }