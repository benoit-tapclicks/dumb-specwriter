<?php

namespace App\Middleware;

class AuthAdminMiddleware extends Middleware
{
    public function __invoke($request, $response, $next)
    {
        if(!$this->auth->isLoggedIn())
        {
            $_SESSION['errors']['login'][] = 'Your session has expired';
            return $response->withRedirect('/login');
        }
        elseif (!$this->auth->isAdmin())
        {
            $notFoundHandler = $this->container->get('notFoundHandler');
            return $notFoundHandler($request, $response);
        }

        $response = $next($request, $response);
        return $response;

    }
}