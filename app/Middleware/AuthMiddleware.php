<?php

namespace App\Middleware;

class AuthMiddleware extends Middleware
{
    public function __invoke($request, $response, $next)
    {
        if(!$this->auth->isLoggedIn())
        {
            $_SESSION['errors']['login'][] = 'Your session has expired';
            return $response->withRedirect('/login');
        }

        $response = $next($request, $response);
        return $response;

    }
}