<?php

namespace App\Middleware;

class AuthViewMiddleware extends Middleware
{
    public function __invoke($request, $response, $next)
    {
        $this->view->getEnvironment()->addGlobal('auth', $this->auth);
        $response = $next($request, $response);
        return $response;
    }
}