$(document).ready(function(){


    $(document).on('click', '.delete',function()
    {
        post = {
            "user_id": $(this).data('user_id'),
            "csrf_name": $("#token_name").val(),
            "csrf_value": $("#token_value").val()
        };


        $.post('/admin/users/delete',post).done(function(data)
        {
            data = JSON.parse(data);
            if(data.status == "success")
            {
                $('[data-user_id="' + data.user_id + '"]').remove();
            }
        }).fail(function(a){
            console.log(a);
        });
    });
});