$(document).ready(function(){

    self.grid_id = $("#grid_id").val();

    $(".check-all").change(function () {
        $('.cb').prop('checked', $(this).prop("checked"));
    });

    $(document).on('click', '#copy', function(){

        var ids = [];

        $('.cb').each(function(){

            if(this.checked)
            {
                ids.push($(this).parents('tr').data('item_id'));
            }
        });

        post = {
            "csrf_name": $("#token_name").val(),
            "csrf_value": $("#token_value").val(),
            "ids": ids
        };

        $.post('/items/clipboard/copy', post).done(function(data) {
            data = JSON.parse(data);
            console.log(data);
        }).fail(function(a,b,c){});
    });

    $(document).on('click', '#paste', function(){

        post = {
            "csrf_name": $("#token_name").val(),
            "csrf_value": $("#token_value").val(),
            "grid_id": self.grid_id,
        };

        $.post('/items/clipboard/paste', post).done(function(data) {
            data = JSON.parse(data);
            location.reload();
            console.log(data);
        }).fail(function(a,b,c){});
    });

    $(document).on('click', '.editable', function(){

        if($(this).html().match(/^<input type="text"/) || $(this).html().match(/^<input value="/))
        {
            return;
        }

        var td = $(this);
        var current_html = td.html();
        var input = '<input type="text" value="' + current_html + '">';
        td.html(input);
        td.children()[0].selectionStart = 0;
        td.children()[0].selectionEnd = current_html.length;
        td.children().focus();

        $(document).keypress(function(e) {
            if(e.which == 13) {

                $(document).unbind('keypress');

                var item_id = td.parent().data('item_id');
                var value = td.children().val();
                var field = td.data('field');

                post = {
                    "csrf_name": $("#token_name").val(),
                    "csrf_value": $("#token_value").val(),
                    "grid_id": self.grid_id,
                    "field": field,
                    "item_id": item_id,
                    "value": value
                };
                $.post('/grid/' + self.grid_id + '/item/edit', post).done(function(){
                    td.html(value);
                });
            }
        });
    });
    $(document).on('click', '.yn', function(){

        var td = $(this);
        var yn = (td.html().length > 0) ? 0 : 1;
        var field = td.data('field');
        var item_id = td.parent().data('item_id');

        var radio = {
            "is_metric": "is_dimension",
            "is_dimension": "is_metric"
        };

        var glyph = ['', '<span class="text-success glyphicon glyphicon-ok"></span>'];

        var  radio_value = [1,0];

        post = {
            "csrf_name": $("#token_name").val(),
            "csrf_value": $("#token_value").val(),
            "grid_id": self.grid_id,
            "field": field,
            "item_id": item_id,
            "yn": yn
        };

        $.post('/grid/' + self.grid_id +'/item/yn', post).done(function(){

            content = glyph[yn];
            td.html(content);

            if(radio[field] != undefined)
            {
                td.parent('tr').children('td[data-field='+ radio[field] +']').html(glyph[radio_value[yn]]);
            }

        }).fail(function(a,b,c){

        });
    })

    $(document).on('click', '#delete',function(){
        var ids = [];

        $(':checkbox').each(function()
        {
            if(this.checked)
            {
                item_id = $(this).parents('tr').data('item_id');

                if(item_id != undefined)
                {
                    ids.push(item_id);
                }
            }
        });

        post = {
            "csrf_name": $("#token_name").val(),
            "csrf_value": $("#token_value").val(),
            "grid_id": self.grid_id,
            "items": ids
        };
        $.post('/grid/' + self.grid_id + '/items/delete', post).done(function(data){
            data = JSON.parse(data);

            if(data.error == false)
            {
                for(var i = 0; i < data.items.length; i++)
                {
                    item_id = data.items[i];
                    $('tr[data-item_id=' + item_id + ']').remove();
                }
            }
            $('.check-all').prop('checked', false);

        }).fail(function(a,b,c){

        });
    });

    $(document).on('click', '.new',function(){

        self.action = $(this).data('action');

        if(self.action == 'add')
        {
            self.parent_row = $('.tbody').last();
        }
        else
        {
            self.parent_row = $(this).parents('tr');
        }

        post = {
            "csrf_name": $("#token_name").val(),
            "csrf_value": $("#token_value").val(),
            "grid_id": self.grid_id
        };

        $.post('/grid/' + self.grid_id + '/item/insert', post).done(function(data)
        {
            data = JSON.parse(data);
            if(self.action == 'insert')
            {
                self.parent_row.after(data);
            }
            else
            {
                self.parent_row.append(data);
            }
            $('#sortable').trigger('updatesort');
            $('[data-toggle="tooltip"]').tooltip();
        });
    });

    $( "#sortable" ).sortable({

        items: 'tr:not(.unsortable)',
        connectWith: '.connectedSortable',
        stop: function(event, ui){
            var order = [];
            $(this).children("tr").each(function(index)
            {
                item_id = $(this).data('item_id');
                order.push(item_id);

            });

            post = {
                "csrf_name": $("#token_name").val(),
                "csrf_value": $("#token_value").val(),
                "grid_id": self.grid_id,
                "order": order
            };
            $.post('/grid/' + self.grid_id + '/items/sort', post).done(function(data){}).fail(function(a,b,c){});
        }
    }).on('updatesort',function() {

        var order = [];

        $('#sortable').children("tr").each(function(index) {
                item_id = $(this).data('item_id');
                order.push(item_id);
            }
        );

        post = {
            "csrf_name": $("#token_name").val(),
            "csrf_value": $("#token_value").val(),
            "grid_id": self.grid_id,
            "order": order
        };
        $.post('/grid/' + self.grid_id + '/items/sort', post).done(function(data){}).fail(function(a,b,c){});
    });
});